# Office LaTeX Suite

A couple of LaTeX classes written by myself to easily set up documents and presentations.

## defaultDoc
A LaTeX class with a couple of functions and configurations to start with a simple document.

## Defaults
* Size: A4
* Margins all sites: 25mm
* Font: Helvetica
* Font-size: 11pt

## Functions

### Start a defaultDoc document
When you start a defaultDoc document you can use the environment called `defaultDoc`. The \begin{} environment will create all you need with the right page numbering:
* Start the document
* Set title, date and authors
* Adds title page with optional image
* Adds table of contents on new page

The \end{} environment closes the document with a bibliography if you use it:
* Uses .bib file from the `\setBibfile` command
* Ends the document

```latex
\setBibfile{references}

\begin{defaultDoc}[optional image]{title}{author}

% rest of your document

\end{defaultDoc}
```

If you don't want to use the defaulDoc environment and just want a normal start you can still use the custom title page and table of contents with:
```latex
\title{tile}
\date{\today}
\author{authors}

\setBibfile{references}

\begin{document}
\beginDocument[optional image]

% rest of your document

\insertBibliography
\end{document}
```

### Insert centered image
You must have a caption to be able to label an image in LaTeX. You can use image like *.png or *.jpg, but if you want a svg like image you must first convert it to a pdf image. This pdf image can be used with `\centerImage`. 
```latex
\centerImage[width=\linewidth, label=\empty]{image}{caption}
```
You can also set this command somewhere in the top of your document:
```latex
\imageDir{Path to directory}
``` 
Now you can instead of typing `./img/example.jpg`, just type `example` and it will load the image.

### Insert multiple images together
You can also have multiple images together next to each other or under each other. Watch out when you add blank spaces between subImages because that makes the images go under each other instead of being next to each other.
```latex
\begin{multiImage}[label]{multiImage caption}
    \subImage[0.3\linewidth]{image1}{image1caption}
    \subImage[0.3\linewidth]{image2}{image2caption}
    \subImage[0.3\linewidth]{image3}{image3caption}
\end{multiImage}
```

### Insert a whitespace
This will insert a normal whitespace and should work in any place in text.
```latex
\whitespace
```

### Clickable url text
A simple clickble href with a default blue color.
```latex
\customhref[color=blue]{url}{text}
```

### Bibliography
defaultDoc will add a bibliography at the end of your document. This is will be generated with BibTex.
It uses default the APA standard. You can set the .bib file at the top of your document like this:
```latex
\setBibFile{references}
```
Default value uses the `references.bib` file in the same folder as the .tex with the defaultDoc class.
