%% beamerDoc
%% Copyright (C) 2020  Bram van Donselaar
%% E-mail: bvdteam13gmail.com
%%
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <http://www.gnu.org/licenses/>.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{beamerDoc}[A Powerpoint like document format with easy to use commands]

\LoadClass{beamer}

\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[official]{eurosym}
\RequirePackage[scaled]{helvet}
\RequirePackage{keyval}
\RequirePackage{tikz}
\usetikzlibrary{positioning}

\renewcommand{\familydefault}{\sfdefault} 

%% \customhref, blue href unless specified else
\newcommand{\customhref}[3][blue]{\href{#2}{\color{#1}{\underline{#3}}}}

%% \imageDir{Path to directory}
\newcommand{\imageDir}[1]{ \graphicspath{ {#1} }}

%% Image positioning (x,y)
%%         0,1
%%          |
%%          |
%% -1,0----0,0----1,0
%%          |
%%          |
%%         0,-1

%% \insertImage[width, x, y]{image}{caption}
\define@key{insertImage}{height}{\def\ii@myheight{#1}}
\define@key{insertImage}{x}{\def\ii@myX{#1}}
\define@key{insertImage}{y}{\def\ii@myY{#1}}
\setkeys{insertImage}{height=2cm, x=0cm, y=0cm}

\newcommand{\insertImage}[3][]{%
  \begingroup
  \setkeys{insertImage}{#1}
  \def\myimage{#2}
  \def\mycaption{#3}

  \begin{tikzpicture}[remember picture,overlay]  
    \node[xshift=\ii@myX, yshift=\ii@myY] (a) at (current page.center)
         {\includegraphics[height=\ii@myheight]{\myimage}};
         
    \if\mycaption\empty
    \else
         \node[below left= \ii@myheight/2 and -0.5cm of a, xshift=\ii@myX, yshift=\ii@myY] at (current page.center)
              {\tiny Figure: \mycaption};
    \fi
  \end{tikzpicture}
  \endgroup
}
