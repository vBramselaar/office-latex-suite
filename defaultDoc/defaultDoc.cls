%% defaultDoc
%% Copyright (C) 2020  Bram van Donselaar
%% E-mail: bvdteam13@gmail.com
%% 
%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.
%% 
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
%% GNU General Public License for more details.
%% 
%% You should have received a copy of the GNU General Public License
%% along with this program.	 If not, see <http://www.gnu.org/licenses/>.

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{defaultDoc}[A Word like document format with easy to use commands]

\LoadClass[a4paper, 11pt]{article}
%% Rendering
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
%% Figures
\RequirePackage{graphicx}
%% Font
\RequirePackage[scaled]{helvet}
%% Margins
\RequirePackage[top=25mm, bottom=25mm, left=25mm, right=25mm]{geometry}
%% Sub figures
\RequirePackage{subcaption}
%% Wrapped figures
\RequirePackage{wrapfig}
%% Href color
\RequirePackage[colorlinks = true,
linkcolor = black,
urlcolor  = black,
citecolor = black,
anchorcolor = black]{hyperref}
%% APA style references
\RequirePackage{apacite}
%% Add another pdf to your document
\RequirePackage{pdfpages}
%% custom arguments for functions
\RequirePackage{keyval}

\renewcommand{\familydefault}{\sfdefault}

%% BibTex linespacing
\setlength\bibitemsep{\baselineskip}

%% To set BibTex file
\def\setBibFile#1{\def\@currentBibFile{#1}}
\setBibFile{references} %% default is "references.bib"

%% Default img directory
\graphicspath{ {img/} }

\newcommand{\beginDocument}[1][\empty]
{%
  \hypersetup{pageanchor=false}
  \pagenumbering{gobble}
  % 
  \begin{titlepage}
	\maketitle
	% 
	\ifx#1\empty
	\else
	\centerImage[width=0.8\linewidth]{#1}{}
	\fi
  \end{titlepage}
  % 
  \tableofcontents
  \clearpage
  % 
  \hypersetup{pageanchor=true}
  \pagenumbering{arabic}
}

\newcommand{\insertBibliography}
{%
  \renewcommand\bibname{Bibliography}
  \renewcommand\refname{Bibliography}
  \bibliographystyle{apacite}
  \bibliography{\@currentBibFile}
}

%% \begin{defaultDoc}[image]{Title}{Author}
\newenvironment{defaultDoc}[3][\empty]%
{%
  \title{#2}
  \date{\today}
  \author{#3}

  \document
  \beginDocument[#1]
}%
{%
  \clearpage
  \insertBibliography
  \enddocument
}

%% \customhref[color=blue]{url}{text}
\newcommand{\customhref}[3][blue]{\href{#2}{\color{#1}{\underline{#3}}}}

%% \whiteSpace, gives a whitespace like 2 return presses
\newcommand{\whitespace}{\vspace*{\baselineskip} \ \\}

%% \centerImage[width=\linewidth, label=\empty]{image}{caption}
\define@key{centerImage}{width}{\def\ci@mywidth{#1}}
\define@key{centerImage}{label}{\def\ci@mylabel{#1}}
\setkeys{centerImage}{width=\linewidth, label=\empty}

\newcommand{\centerImage}[3][]{%
  \begingroup
  \setkeys{centerImage}{#1}
  \def\myimage{#2}
  \def\mycaption{#3}
  % 
  \begin{figure}[!htbp]
	\centering
	\includegraphics[width=\ci@mywidth]{\myimage}
    % 
	\if\mycaption\empty
	\else
	\caption{\mycaption}
	\fi
	% 
	\if\ci@mylabel\empty
	\else
	\label{fig:\ci@mylabel}
	\fi
  \end{figure}
  \endgroup
}


%% \begin{multiImage}[label]{caption}
\newenvironment{multiImage}[2][\empty]%
{%
  \def\multiMylabel{#1}
  \def\multiMycaption{#2}
  % 
  \begin{figure}[!htbp]
	\centering
  }%
  {%
	\if\multiMycaption\empty
	\else
	\caption{\multiMycaption}
	\fi
	% 
	\if\multiMylabel\empty
	\else
	\label{fig:\multiMylabel}
	\fi
	% 
  \end{figure}
}

%% \subImage[width]{image}{caption}
\newcommand{\subImage}[3][\linewidth]{%
  \def\subWidth{#1}
  \def\subMyimage{#2}
  \def\subMycaption{#3}
  % 
  \begin{subfigure}[b]{\subWidth}
	\includegraphics[width=\linewidth]{\subMyimage}
	% 
	\if\subMycaption\empty
	\else
	\caption{\subMycaption}
	\fi
  \end{subfigure}
}
